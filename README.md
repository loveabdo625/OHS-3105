<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>
SPDX-FileCopyrightText: 2021 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Welcome to DIN SPEC 3105's community hub

**You will find here:**

- the latest version of the community version of the [DIN SPEC 3105](
  https://www.beuth.de/de/technische-regel/din-spec-3105-1/324805763);
- information to understand what DIN SPEC 3105 is and how to use it;
- ways to participate in the further development of this standard.

**Quick guide:**

- You don't know what DIN SPEC 3105 is?
  Check our [FAQ](https://gitlab.com/OSEGermany/OHS-3105-aux/-/blob/master/FAQ.md).
- You are an Open Source hardware developer and would like to know
  what DIN SPEC 3105 means for your hardware?
  Check our [best practice](https://gitlab.com/OSEGermany/OHS-3105-aux/-/blob/master/DIN_SPEC_3105-1_best-practice.md)
- You would like to participate in the development
  of the next version of DIN SPEC 3105?
  Check our [contribution guide](CONTRIBUTING.md).

**In short:**

DIN SPEC 3105 delivers a rigourous definition of the "Open Source Hardware",
and therewith makes a concrete proposition to solve an open question:
what is the "source" of open source hardware.

**The main documents in this repository are:**

The main (source) files and their generated versions:

| | this version | latest DIN release \[official\] | latest OHS release | development |
| ---- | --- | --- | --- | --- |
| _git reference_ | [`${PROJECT_VERSION}`](.) | [`din`](https://gitlab.com/OSEGermany/OHS-3105/-/tree/din/) | [`ohs`](https://gitlab.com/OSEGermany/OHS-3105/-/tree/ohs/) | [`develop`](https://gitlab.com/OSEGermany/OHS-3105/-/tree/develop/) |
| __3105-1__ | [source](DIN_SPEC_3105-1.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/din-spec-3105-1/) – [PDF](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/DIN_SPEC_3105-1.pdf) | [source](https://gitlab.com/OSEGermany/OHS-3105/-/tree/din/DIN_SPEC_3105-1.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/din/din-spec-3105-1/) – [PDF](https://osegermany.gitlab.io/OHS-3105/din/DIN_SPEC_3105-1.pdf) | [source](https://gitlab.com/OSEGermany/OHS-3105/-/tree/ohs/DIN_SPEC_3105-1.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/ohs/din-spec-3105-1/) – [PDF](https://osegermany.gitlab.io/OHS-3105/ohs/DIN_SPEC_3105-1.pdf) | [source](https://gitlab.com/OSEGermany/OHS-3105/-/tree/develop/DIN_SPEC_3105-1.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/develop/din-spec-3105-1/) – [PDF](https://osegermany.gitlab.io/OHS-3105/develop/DIN_SPEC_3105-1.pdf) |
| __3105-2__ | [source](DIN_SPEC_3105-2.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/din-spec-3105-2/) – [PDF](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/DIN_SPEC_3105-2.pdf) | [source](https://gitlab.com/OSEGermany/OHS-3105/-/tree/din/DIN_SPEC_3105-2.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/din/din-spec-3105-2/) – [PDF](https://osegermany.gitlab.io/OHS-3105/din/DIN_SPEC_3105-2.pdf) | [source](https://gitlab.com/OSEGermany/OHS-3105/-/tree/ohs/DIN_SPEC_3105-2.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/ohs/din-spec-3105-2/) – [PDF](https://osegermany.gitlab.io/OHS-3105/ohs/DIN_SPEC_3105-2.pdf) | [source](https://gitlab.com/OSEGermany/OHS-3105/-/tree/develop/DIN_SPEC_3105-2.md) – [HTML](https://osegermany.gitlab.io/OHS-3105/develop/din-spec-3105-2/) – [PDF](https://osegermany.gitlab.io/OHS-3105/develop/DIN_SPEC_3105-2.pdf) |
| diff to ... | [din](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/git_diff_side_din--${PROJECT_VERSION}.html) – [ohs](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/git_diff_side_ohs--${PROJECT_VERSION}.html) – [develop](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/git_diff_side_develop--${PROJECT_VERSION}.html) | [ohs](https://osegermany.gitlab.io/OHS-3105/din/git_diff_side_ohs--din.html) – [develop](https://osegermany.gitlab.io/OHS-3105/din/git_diff_side_develop--din.html) | [din](https://osegermany.gitlab.io/OHS-3105/ohs/git_diff_side_din--ohs.html) – [develop](https://osegermany.gitlab.io/OHS-3105/ohs/git_diff_side_develop--ohs.html) | [din](https://osegermany.gitlab.io/OHS-3105/develop/git_diff_side_din--develop.html) – [ohs](https://osegermany.gitlab.io/OHS-3105/develop/git_diff_side_ohs--develop.html) |
| ___all output___ | [index](https://osegermany.gitlab.io/OHS-3105/${PROJECT_VERSION}/index.html) | [index](https://osegermany.gitlab.io/OHS-3105/din/index.html) | [index](https://osegermany.gitlab.io/OHS-3105/ohs/index.html) | [index](https://osegermany.gitlab.io/OHS-3105/develop/index.html) |

For __FAQ__, __OSH Best Practice__ and __Meeting Minutes__,
see [the equivalent section in the _aux_ repo](https://gitlab.com/OSEGermany/OHS-3105-aux/-/blob/master/README.md#source-export)

## Naming

The documents

- DIN SPEC 3105-1
- DIN SPEC 3105-2
- OHS 3105-1
- OHS 3105-2

are adoptions of [DIN SPEC 3105-1](https://www.beuth.de/en/technical-rule/din-spec-3105-1/324805763)
and [DIN SPEC 3105-2](https://www.beuth.de/en/technical-rule/din-spec-3105-2/324805750),
respectively, by [DIN e.V.](https://www.din.de/en/din-and-our-partners/din-e-v)
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
This first publishing of an official standard under a free/open license is enabled
by DIN's pilot project to approach open source communities and the great collaboration
of lots of different actors and activists of the OSH community (huge thanks again!).

As indicated,
you will find two mutually exclusive prefixes in file names in this repository:

- `DIN SPEC`\
  for adopted work whose content is identical with the original document
  - The files here are the original source files
    which then have been published by the licensor DIN e.V. with altered layout;
    the content itself however remained unchanged.
- `OHS` (Open Hardware Standard)\
  for adopted work with _changed_ content (e.g. after further development)
  - we use this alternative prefix in order to avoid confusion and trademark infringement.

## What is it?

**DIN SPEC 3105-1**
delivers an unambiguous and operational definition of the
concept of open source hardware and breaks it down into objective
criteria for judging the compliance of a piece of hardware with this
definition.

**DIN SPEC 3105-2**
defines requirements for implementing a community-based certification procedure
for Open Source Hardware.
It aims at groups or persons willing to build a certification procedure
as well as groups or persons willing to attest the compliance
of the documentation of a piece of hardware with the requirements
set in DIN SPEC 3105-1.

This is the first standard published
by DIN e.V. under a free/open license. Following the principles of open
source, anybody can contribute to its further development online.
Please do :)
(following our [contribution guide](CONTRIBUTING.md)).

## Development

### Get the documentation build-tool {#get-build-tool}

Make sure you have the [MoVeDo](https://github.com/movedo/MoVeDo)
documentation build tool checked out:

If you have not yet cloned this repo,
you can do so,
plus get MoVeDo in one go with:

```bash
git clone --recurse-submodules https://gitlab.com/OSEGermany/OHS-3105.git
cd OHS-3105
```

If you already have this repo locally,
you can switch to it and get MoVeDo like this:

```bash
cd OHS-3105
git submodule update --init --recursive
```

### How to generate output

If you have MoVeDo available locally as [described above](#get-build-tool),
simply run:

```bash
movedo/scripts/build
```

## Imprint

<https://www.ose-germany.de/impressum/>
